(function() {

    function htmlToElement(html) {
      var template = document.createElement('template');
      html = html.trim();
      template.innerHTML = html;
      return template.content.firstChild;
    }
    function insertAfter(el, referenceNode) {
      referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
    }
    var base = document.body.getElementsByClassName('profile_rightcol')[0];
    if (base.getElementsByClassName('profile_ban_status')[0]) return;
    var VACBan = htmlToElement(
        '<div class="profile_ban_status"><div class="profile_ban">1 VAC ban on record <span class="profile_ban_info">| <a class="whiteLink" href="https://support.steampowered.com/kb_article.php?ref=7849-Radz-6869&amp;l=english" target="_blank" rel="noreferrer">Info</a></span></div>0 day(s) since last ban</div>');
    if (!base.firstElementChild)
      base.appendChild(VACBan);
    else
      insertAfter(VACBan, base.firstElementChild);
})();